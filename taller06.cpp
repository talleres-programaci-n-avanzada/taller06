#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>

using namespace std;

void generarVector(int filas, int numeros, int vector[]);
void ordenamiento(int cifras, int numeros, int **matriz, int vector[], int digito);

int main(void) {
    srand(time(NULL));

    int filas, numeros, cifras;
    short opcion = 0;

    cout << "Ordenamiento por cubetas: \n";
    cout << "Ingrese la cantidad de cifras máxima:  \n";
    cin >> filas;
    cifras = pow(10, filas) - 1;
    numeros = rand() % 100;

    cout << "Los valores de su vector estarán generados entre 0 y " << cifras << ", y tendrá " << numeros << " números.\n";
    int *vector = new int[numeros];
    generarVector(cifras, numeros, vector);
    int **matriz = new int *[10];

    int i = 0;
    int opciones_elegidas = 0; 

    while (opciones_elegidas < filas) {
        cout << "Seleccione la opción que desee: \n";
        cout << "1. Imprimir matriz en siguiente paso de ordenamiento y trasladar valores al vector para ordenar\n";
        cout << "2. Salir. \n";
        cin >> opcion;

        if (opcion == 1) {
            ordenamiento(cifras, numeros, matriz, vector, i);
            i++;
            opciones_elegidas++;
        }
    }

    delete[] vector;
    for (int i = 0; i < 10; i++) {
        delete[] matriz[i];
    }
    delete[] matriz;

    return 0;
}

void generarVector(int filas, int numeros, int vector[]) {
    for (int i = 0; i < numeros; i++) {
        vector[i] = 1 + rand() % filas;
        cout << vector[i] << "|";
    }
    cout << "\n\n";
}

void ordenamiento(int cifras, int numeros, int **matriz, int vector[], int digito) {
    int *vector_columnas = new int[10]{0};

    for (int j = 0; j < numeros; j++) {
        int cubeta = (vector[j] / (int)pow(10, digito)) % 10;
        vector_columnas[cubeta]++;
    }

    for (int i = 0; i < 10; i++) {
        matriz[i] = new int[vector_columnas[i]];
    }

    int *posiciones = new int[10]{0};
    for (int j = 0; j < numeros; j++) {
        int cubeta = (vector[j] / (int)pow(10, digito)) % 10;
        int posicion = posiciones[cubeta]++;
        matriz[cubeta][posicion] = vector[j];
    }
    
    int k = 0;
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < vector_columnas[i]; j++) {
            vector[k] = matriz[i][j];
            k++;
        }
    }
    
    cout << "Matriz:\n";
    for (int i = 0; i < 10; i++) {
        cout << i << ": ";
        for (int j = 0; j < vector_columnas[i]; j++) {
            cout << matriz[i][j] << " ";
        }
        cout << "\n";
    }
    
    cout << "Vector: ";
    for (int j = 0; j < numeros; j++) {
        cout << vector[j] << " | ";
    }
    
    cout << "\n";
    
    delete[] vector_columnas;
    delete[] posiciones;
}