# taller06


El ordenamiento en cubetas inicia con un arreglo unidimensional de longitud N que contiene los enteros positivos a ordenar y una matriz de diez filas por N columnas. Cada fila de la matriz es considerada una cubeta.

El funcionamiento del algoritmo es muy sencillo, primero tome cada elemento del vector y basado en el número de unidades del valor; colóquelo en la fila de la matriz indicada por las unidades del número. Por ejemplo, el valor 97 se colocará en la fila 7 de la matriz, 3 se colocará en la fila 3 y 100 se colocará en la fila O.

Cada número que se deba ubicar en la misma fila lo hará en la columna inmediatamente disponible, por ejemplo 97 y 17 son valores que deben ubicarse en la fila 7 en la primera y segunda columna respectivamente. Ahora debe recorrer toda la matriz por filas e ir ubicando los elementos en el arreglo unidimensional. Se debe repetir este proceso por las decenas, centenas, millares, etc.

Elabore un programa en C++ utilizando apuntadores y memoria dinámica para la creación de los vectores y las matrices en cada uno de los pasos, el cual debe generar el vector ordenado. El vector inicial debe ser creado dinámicamente, solicitándole inicialmente al usuario el número máximo de cifras para los elementos del vector y generarlos aleatoriamente. Por ejemplo: si el usuario digita 3 los números estarán entre 0 y 999.

## Condiciones que debe cumplir el programa:
1. Crear vector inicial dinámicamente. (1.0)
2. En cada paso crear el vector y la matriz dinámicamente (usando delete cuando se 
requiera)(3.0)
3. Proceso de ordenamiento de los números de acuerdo con la explicación dada (1.0)